﻿Shader "Unlit/Role"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        _LightPos ("LightPos", vector) = (0, 0, 0, 0)
        _RimCol ("RimCol", Color) = (1, 1, 1, 1)
        _RimCol2 ("RimCol2", Color) = (1, 1, 1, 1)
        _RimPow ("RimPow", Range(1, 32)) = 1
        _RimIntensity ("RimIntensity", Range(1, 10)) = 1
        _RimLerp ("RimLerp", Range(0, 1)) = 0
        _ReflectCol ("ReflectCol", color) = (1, 1, 1, 1)
        _ReflectPow ("ReflectPow", Range(1, 32)) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            Tags { "LightMode" = "ForwardBase" }
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv: TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex: SV_POSITION;
                float3 normal: TEXCOORD1;
                float4 localPos: TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST, _LightPos;
            fixed4 _RimCol, _RimCol2, _ReflectCol;
            float _RimPow, _RimIntensity, _ReflectPow, _RimLerp;

            v2f vert(appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                UNITY_TRANSFER_FOG(o, o.vertex);
                o.normal = v.normal;
                o.localPos = v.vertex;
                return o;
            }

            fixed4 frag(v2f i): SV_Target
            {
                float3 viewPos = normalize(WorldSpaceViewDir(i.localPos));
                float3 lightPos = UnityObjectToWorldDir(normalize(_LightPos - i.localPos));
                float3 normal = normalize(UnityObjectToWorldNormal(i.normal));
                float3 reflectPos = normalize(reflect(lightPos, normal));

                float dotV = pow(1 - saturate(dot(viewPos, normal)), _RimPow);
                float dotR = pow(saturate(dot(reflectPos, viewPos)), _ReflectPow);
                float dotD = 1 - (dot(lightPos, normal) * 0.5 + 0.5);

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) ;//* dotD * 1.5;
                col.rgb += lerp(_RimCol.rgb, _RimCol2.rgb, _RimLerp) * dotV * _RimIntensity;// + _ReflectCol * dotR;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
            
        }
    }
    Fallback "Diffuse"
}
