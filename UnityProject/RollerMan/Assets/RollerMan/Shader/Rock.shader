﻿Shader "Unlit/Rock"
{
    Properties
    {
        _Texture ("Texture", 2D) = "white" { }
        _MossTexture ("Moss Texture", 2D) = "white" { }
        _FallOff ("FallOff", Range(0, 100)) = 100
        _Mask ("Mask", 2D) = "white" { }

        [HideInInspector]_Black ("Black", 2D) = "white" { }

        _LightPos ("LightPos", vector) = (0, 0, 0, 0)
        _Intensity ("Intensity", Range(1, 3)) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+0" }

        Pass
        {
            Tags { "LightMode" = "ForwardBase" }
            
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 vertex: SV_POSITION;
                float3 normal: TEXCOORD1;
                float4 localPos: TEXCOORD2;
            };

            uniform sampler2D _MossTexture;
            float4 _MossTexture_ST;
            uniform sampler2D _Texture;
            uniform float4 _Texture_ST;
            uniform sampler2D _Mask;
            uniform sampler2D _Black;
            uniform float _FallOff;

            float4 _LightPos;
            float _Intensity;

            v2f vert(appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MossTexture);
                o.localPos = v.vertex;
                o.normal = v.normal;

                return o;
            }


            fixed4 frag(v2f i): SV_Target
            {
                float3 lightPos = normalize(_LightPos - i.localPos);
                float3 normal = normalize(UnityObjectToWorldNormal(i.normal));

                float dotD = dot(lightPos, normal) * 0.5 + 0.5;
                float dotN = 1 - saturate(dot(fixed3(0, 1, 0), normal));

                fixed4 col = lerp(tex2D(_MossTexture, i.uv), tex2D(_Texture, i.uv), dotN);
                return col * dotD * _Intensity;
            }
            ENDCG
            
        }
    }
}
