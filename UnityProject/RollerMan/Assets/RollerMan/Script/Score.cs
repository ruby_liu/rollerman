﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class Score : MonoBehaviour
{
    public int score = 0;

    void Awake()
    {
        TextMeshPro tmp = GetComponentInChildren<TextMeshPro>();
        if (tmp != null)
        {
            tmp.text = "x" + score;
        }
    }
}
