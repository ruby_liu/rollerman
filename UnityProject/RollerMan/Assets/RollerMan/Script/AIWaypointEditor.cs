using UnityEngine;
using System.Collections;
using System;


public class AIWaypointEditor : MonoBehaviour
{

    public string folderName = "WayPoints";
    public string preName = "WayPoint";
    public Material waypointMaterial;
    public bool batchCreating = false;


    [ContextMenu("������")]
    public void Rename()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).name = "WP_" + (i + 1);
        }
    }
    [ContextMenu("�л���ʾ")]
    public void ShowOrHide()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Renderer ren = transform.GetChild(i).GetComponent<Renderer>();
            ren.enabled = !ren.enabled;
        }
    }

}
