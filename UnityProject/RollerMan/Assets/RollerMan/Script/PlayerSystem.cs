﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MidnightCoder.Game;
using UnityEngine;
using Random = UnityEngine.Random;
using Timer = MidnightCoder.Game.Timer;

public class PlayerSystem : TMonoSingleton<PlayerSystem>
{
    public bool navOpen = true;
    public Renderer[] allRenders;
    public AnimationCurve curve;
    public Transform body;
    public CameraCtl cameraCtl;
    public Animator ainm;
    public CharacterController cc;
    public ParticleSystem[] snowFx;
    public ParticleSystem[] fireFx;

    public ParticleSystem[] accFx;
    public float moveSpeed = 3;
    public float maxMoveSpeed = 3;
    public float rotateSpeed = 3;
    public float lrSpeed = 1;
    public Vector3 moveDir;
    public Vector3 desDir;
    public bool startMove = false;
    public float gravity = 2;
    public float jumpGravity = -40;
    public float tempJumpGravity = -40;
    private int jumpFallSpeed = 50;
    private Queue<Transform> jumpTrackTrans;

    public SpeedType speedType = SpeedType.FAST;

    private float accTimer = 2;

    public bool getJump = false;
    public bool inJump = false;

    public Transform wayPoint;
    public int curWayIndex = 0;

    public bool inAccelerate = false;
    public float accelerateTimer = 2;
    public float accelerateInterval = 2;

    public float maxCameraLROffsetValue = 0.5f;

    public float maxCameraLRRotateValue = 20f;

    public bool getInJumpArea = false;

    private float curTrunValue = 0;
    private bool inCar = false;
    private bool getScoreBoard = false;
    private bool isFinish = false;
    public int curRank = -1;
    private bool inSnow = false;
    private bool keepTouching = false;

    private bool isDie = false;
    public Transform explosionPos;
    public GameObject huangguan;
    void Start()
    {
        jumpTrackTrans = new Queue<Transform>();
        cameraCtl.targetFollow = this.transform;
        cameraCtl.targetRot = this.transform;
        cameraCtl.SetReady();

        DoShakeHand();
    }
    private void DoShakeHand()
    {

    }

    void Update()
    {
        huangguan.transform.Rotate(Vector3.up, 1f);
        if (isDie)
        {
            //cameraCtl.UpdateFollowCameraPos2();
            return;
        }
        if (Input.GetMouseButtonDown(0) && !TouchX.IsPointerOverUIObject(Input.mousePosition) && !startMove && !getScoreBoard)
        {
            this.StartMove();
        }
        if (!isFinish)
        {
            UIManager.S.UpdateSpeedProgressValue((moveSpeed - 10) / (50));
            curRank = EnemySpawner.S.enemyFrontPlayer.Count + 1;
            UIManager.S.UpdateGameRankShow(curRank);
            huangguan.SetActive(curRank == 1);
            keepTouching = true;//Input.GetMouseButton(0);
        }
        else
        {
            keepTouching = true;
        }

        if (inJump)
        {
            SoundManager.S.PlayRollSound(1, SoundType.PlayerRollFly);
        }
        else
        {
            SoundManager.S.PlayRollSound(Mathf.Clamp((moveSpeed - 10) / (maxMoveSpeed - 10), 0, 0.7f), inSnow ? SoundType.PlayerRollSnow : SoundType.PlayerRollRoad);
        }
    }
    public Transform rayPoint;
    void FixedUpdate()
    {
        if (isDie) return;
        if (this.startMove)
        {
            if (inAccelerate)
            {
                accelerateTimer -= Time.fixedDeltaTime;
                if (accelerateTimer < 0)
                {
                    maxMoveSpeed = 40;
                    //cameraCtl.blur.enabled = false;
                    inAccelerate = false;
                    ainm.SetBool("Accelerate", false);
                    PlayAccFx(false);
                    foreach (Renderer ren in allRenders)
                    {
                        ren.material.SetFloat("_RimLerp", 0);
                    }
                    accelerateTimer = accelerateInterval;
                }
            }
            else
            {
                cameraCtl.blur.blurScale = 0.0035f * (moveSpeed - 10) / (maxMoveSpeed - 10);
            }


            if (!getJump)
            {
                if (navOpen)
                {
                    if (curWayIndex < wayPoint.childCount)
                    {
                        Transform targetPoint = WaypointScript.S.points[curWayIndex];
                        Vector3 dir = targetPoint.position - transform.position;
                        float dotValue = Vector3.Dot(transform.forward, dir);
                        float angleValue = Vector3.Angle(transform.forward, targetPoint.right);

                        //Vector3 crossValue = Vector3.Cross(transform.forward, targetPoint.forward);
                        //Debug.LogError(dotValue + "---" + angleValue);
                        if (dotValue < 100)
                        {
                            if (dotValue > 1)
                            {
                                if (angleValue > 0)
                                {
                                    if (!inJump && !inFlip)
                                    {
                                        Transform nextP = WaypointScript.S.points[curWayIndex];
                                        Quaternion nextRot2 = Quaternion.LookRotation(Vector3.Cross(nextP.forward, Vector3.Cross(transform.forward, nextP.forward)), nextP.forward);
                                        transform.rotation = Quaternion.Lerp(transform.rotation, nextRot2, Time.fixedDeltaTime * 8f);

                                        Quaternion nextRot3 = Quaternion.LookRotation(nextP.right, nextP.forward);

                                        if (moveDir.x == 0)
                                        {
                                            float a = Quaternion.Angle(transform.rotation, nextRot3);
                                            float y = Vector3.Cross(transform.forward, targetPoint.right).y;
                                            a = Mathf.Clamp(a / 6, 0, 1);
                                            int turnDir = y > 0 ? 1 : -1;
                                            curTrunValue = Mathf.Lerp(curTrunValue, a * turnDir, Time.fixedDeltaTime * 4);
                                            ainm.SetFloat("Turn", curTrunValue);
                                            float d = a * turnDir;
                                            cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x + (moveSpeed - 10) / (maxMoveSpeed - 10) * maxCameraLROffsetValue * d, Time.fixedDeltaTime * 2));
                                            this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, (moveSpeed - 10) / (maxMoveSpeed - 10) * maxCameraLRRotateValue * d, this.body.localEulerAngles.z), Time.fixedDeltaTime * 3);
                                            cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, Quaternion.Euler(cameraCtl.idlePos.localEulerAngles.x, (moveSpeed - 10) / (maxMoveSpeed - 10) * (maxCameraLRRotateValue) * d, cameraCtl.idlePos.localEulerAngles.z), Time.fixedDeltaTime * 3);

                                        }
                                        else
                                        {
                                            curTrunValue = Mathf.Lerp(curTrunValue, moveDir.x, Time.fixedDeltaTime * 2);
                                            ainm.SetFloat("Turn", curTrunValue);
                                        }

                                        if (!getInJumpArea)
                                        {
                                            transform.rotation = Quaternion.Lerp(transform.rotation, nextRot3, Time.fixedDeltaTime * curve.Evaluate(angleValue) * 5);
                                        }

                                        if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
                                            1 << LayerMask.NameToLayer("Road")))
                                        {
                                            //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
                                            //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
                                            Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(body.forward, hit.normal)), hit.normal);
                                            body.rotation = Quaternion.Lerp(body.rotation, nextRot, 0.1f);
                                        }
                                    }
                                }
                            }
                            else if (dotValue <= 1)
                            {
                                curWayIndex++;
                            }
                        }
                    }
                }
                else
                {
                    curTrunValue = Mathf.Lerp(curTrunValue, moveDir.x, Time.fixedDeltaTime * 2);
                    ainm.SetFloat("Turn", curTrunValue);
                    if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
                        1 << LayerMask.NameToLayer("Road")))
                    {
                        //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
                        //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
                        Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(transform.forward, hit.normal)), hit.normal);
                        transform.rotation = Quaternion.Lerp(transform.rotation, nextRot, 0.1f);
                        //transform.forward = body.forward;
                    }
                    transform.RotateAround(transform.up, Time.fixedDeltaTime * moveDir.x);
                }
         
                if (inCar)
                {
                    this.lrSpeed = 5.5f;
                }
                else if (getScoreBoard)
                {
                    this.lrSpeed -= Time.fixedDeltaTime * 2;
                }
                else
                {
                    this.lrSpeed = Mathf.Clamp((moveSpeed - 10) / (maxMoveSpeed - 10)*  Mathf.Abs(moveDir.x) * (moveSpeed/2), 1, 20);
                }

                float speedX = moveDir.x * lrSpeed;
                //float speedZ = moveDir.z * moveSpeed;
                // cc.Move((transform.forward + new Vector3(speedX, 0, speedZ) + Vector3.down * 100) * Time.fixedDeltaTime);
                if (inJump)
                {
                    //if (this.body.localEulerAngles.Equals(Vector3.zero))
                    //{
                    //    this.body.SetLocalEulerX(-20);
                    //}
                    ainm.SetFloat("Turn", 0);
                    desDir = (transform.forward * moveSpeed + Vector3.down * tempJumpGravity);
                    tempJumpGravity = Mathf.Clamp(tempJumpGravity + Time.fixedDeltaTime * jumpFallSpeed, -100, 30);
                    //tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;

                    //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));
                    this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(20, this.body.localEulerAngles.y, this.body.localEulerAngles.z), Time.fixedDeltaTime);
                    //this.body.SetLocalEulerX(Mathf.Clamp(this.body.localEulerAngles.x + Time.fixedDeltaTime /10, -20, 20));
                    cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x, Time.fixedDeltaTime * 6));
                }
                else if (inFlip)
                {
                    ainm.SetFloat("Turn", 0);
                    desDir = (transform.forward * moveSpeed + Vector3.down * tempJumpGravity);
                    tempJumpGravity = Mathf.Clamp(tempJumpGravity + Time.fixedDeltaTime * jumpFallSpeed, -100, 30);
                    this.body.Rotate(0, 0, Time.fixedDeltaTime * 400);
                    cameraCtl.idlePos.localPosition = Vector3.Lerp(cameraCtl.idlePos.localPosition, cameraCtl.flipPos.localPosition, Time.fixedDeltaTime * 6);
                }
                else
                {
                    //  this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 0, Time.fixedDeltaTime * 50));
                    desDir = (transform.forward * moveSpeed + Vector3.down * gravity);
                }

                desDir += transform.right * speedX;

                //TODO:处理镜头偏移
                if (speedX == 0)
                {
                    //cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x, Time.fixedDeltaTime * 6));
                    //this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, 0, this.body.localEulerAngles.z), Time.fixedDeltaTime * 6);

                    ////Quaternion nextRot = Quaternion.LookRotation(cameraCtl.idlePos_Standard.forward, cameraCtl.idlePos_Standard.up);
                    ////cameraCtl.idlePos.rotation = Quaternion.Lerp(cameraCtl.idlePos.rotation, nextRot, 0.2f);
                    //cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, Quaternion.Euler(cameraCtl.idlePos.localEulerAngles.x, 0, cameraCtl.idlePos.localEulerAngles.z), Time.fixedDeltaTime * 6);
                }
                else
                {
                    float dir = moveDir.x;
                    if (navOpen)
                        cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x + (moveSpeed - 10) / (maxMoveSpeed - 10) * maxCameraLROffsetValue * dir, Time.fixedDeltaTime * 2));
                    if (!getScoreBoard && navOpen)
                        this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, (moveSpeed - 10) / (maxMoveSpeed - 10) * maxCameraLRRotateValue * dir, this.body.localEulerAngles.z), Time.fixedDeltaTime * 2);
                    if (navOpen)
                    {
                        Quaternion nextRot = Quaternion.LookRotation(body.forward, body.up);
                        cameraCtl.idlePos.rotation = Quaternion.Lerp(cameraCtl.idlePos.rotation, nextRot, 0.2f);
                        cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, Quaternion.Euler(cameraCtl.idlePos.localEulerAngles.x, (moveSpeed - 10) / (maxMoveSpeed - 10) * (maxCameraLRRotateValue - 5) * dir, cameraCtl.idlePos.localEulerAngles.z), Time.fixedDeltaTime * 2);
                    }
                }

                if (getScoreBoard)
                {
                    if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
                        1 << LayerMask.NameToLayer("Road")))
                    {
                        //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
                        //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
                        Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(body.forward, hit.normal)), hit.normal);
                        body.rotation = Quaternion.Lerp(body.rotation, nextRot, 0.1f);
                    }
                }
            }
            else
            {
                if (inJump)
                {
                    //cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x, Time.fixedDeltaTime * 2));
                    //this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, 0, this.body.localEulerAngles.z), Time.fixedDeltaTime * 6);

                    // desDir = (jumpJudgeForward * moveSpeed + Vector3.down * tempJumpGravity);
                    // tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;
                    //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));


                    ainm.SetFloat("Turn", 0);
                    Vector3 flyDir;
                    if (jumpTrackTrans.Count > 0)
                    {
                        Transform ele = jumpTrackTrans.Peek();
                        float dot = Vector3.Dot(transform.forward, (ele.position - transform.position));
                        if (dot < 1)
                        {
                            jumpTrackTrans.Dequeue();
                        }
                        //flyDir = (ele.position - transform.position).normalized;
                        flyDir = ele.forward;
                        transform.forward = Vector3.Lerp(transform.forward, flyDir, Time.fixedDeltaTime * 2);
                    }
                    else
                    {
                        flyDir = transform.forward;
                    }

                    if (jumpTrackTrans.Count < 5)
                    {
                        accelerateTimer = -1;
                    }
                    desDir = flyDir * moveSpeed;
                    desDir += transform.right * moveDir.x * 40;
                    //if (jumpTrackTrans.Count > 0)
                    //{
                    //    Transform ele = jumpTrackTrans.Peek();
                    //    if (Vector3.Dot(transform.forward, (ele.position - transform.position).normalized) < 0)
                    //    {
                    //        jumpTrackTrans.Dequeue();
                    //    }

                    //    Vector3 forwar = (ele.position - transform.position).normalized;
                    //    desDir = forwar * moveSpeed;
                    //    transform.forward = Vector3.Lerp(transform.forward, forwar, Time.fixedDeltaTime * 2);
                    //}


                    //TODO:处理镜头偏移
                    //cameraCtl.idlePos.SetLocalPosX(Mathf.Lerp(cameraCtl.idlePos.localPosition.x, cameraCtl.idlePos_Standard.localPosition.x, Time.fixedDeltaTime * 2));


                    //this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, 0, this.body.localEulerAngles.z), Time.fixedDeltaTime * 3);
                    this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(this.body.localEulerAngles.x, (moveSpeed - 10) / (maxMoveSpeed - 10) * maxCameraLRRotateValue * moveDir.x, this.body.localEulerAngles.z), Time.fixedDeltaTime * 3);

                    //cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, Quaternion.Euler(cameraCtl.idlePos.localEulerAngles.x, 0, cameraCtl.idlePos.localEulerAngles.z), Time.fixedDeltaTime * 6);


                    cameraCtl.idlePos.localPosition = Vector3.Lerp(cameraCtl.idlePos.localPosition, cameraCtl.jumpPos.localPosition, Time.fixedDeltaTime * 2);
                    cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, cameraCtl.jumpPos.localRotation, Time.fixedDeltaTime * 2);
                }
            }

            cc.Move(desDir * Time.fixedDeltaTime);

            if (inJump)
            {
                this.moveSpeed = Mathf.Clamp(moveSpeed + Time.fixedDeltaTime * (5 / Mathf.Clamp(accTimer -= Time.fixedDeltaTime, 0.5f, Mathf.Infinity)), 0, maxMoveSpeed);
            }
            else
            {
                if (!getScoreBoard)
                {
                    int addDir = keepTouching ? 2 : -1;
                    this.moveSpeed = Mathf.Clamp(moveSpeed + Time.fixedDeltaTime * 10 * addDir, 10, maxMoveSpeed);
                }
                else
                {
                    this.moveSpeed = Mathf.Clamp(this.moveSpeed - Time.fixedDeltaTime * 20, 0, 30);
                }
            }

            //if (moveSpeed > 50)
            //{
            //    cameraCtl.cameraFollowSpeed = Mathf.Lerp(cameraCtl.cameraFollowSpeed, Mathf.Clamp(this.moveSpeed , 5, 300), Time.fixedDeltaTime);
            //}
            //else
            //  {
            //   if (!inJump)
            //   {
            if (inAccelerate)
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 100, Time.fixedDeltaTime * 2);
            }
            else
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 90, Time.fixedDeltaTime * 13f);
            }

            //    }

            // }
        }


    }
    private void LateUpdate()
    {
        //cameraCtl.cameraFollowSpeed = Mathf.Lerp(cameraCtl.cameraFollowSpeed, 90, Time.deltaTime * 10);
        cameraCtl.UpdateFollowCameraPos2();

    }
    public bool inFlip = false;

    public void TriggerEnter(Collider other)
    {
        if (isDie) return;
        if (other.tag == "JumpArea")
        {
            this.getInJumpArea = true;
        }

        if (other.tag == "Car")
        {
            inCar = true;
            cameraCtl.SetCar();
            Time.timeScale = 0.65f;
            DOVirtual.DelayedCall(1f, () =>
            {
                Time.timeScale = 1;
            });
        }
        if (other.tag == "GetJump")
        {
            SoundManager.S.PlaySound(SoundType.PlayerJump);
            this.jumpTrackTrans.Clear();
            foreach (Transform child in other.transform)
            {
                this.jumpTrackTrans.Enqueue(child);
            }
            this.getJump = true;
            if (getJump)
            {
                inJump = true;
                //cameraCtl.SetJump();
                //cameraCtl.idlePos.localPosition = Vector3.Lerp(cameraCtl.idlePos.localPosition, cameraCtl.jumpPos.localPosition, Time.fixedDeltaTime * 2);
                //cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, cameraCtl.jumpPos.localRotation, Time.fixedDeltaTime * 2);
                this.accTimer = 2f;
                this.moveSpeed = 40;
                curTrunValue = 0;
                PlayAccFx();
                ainm.SetBool("Accelerate", true);
            }
        }


        if (other.tag == "Jump")
        {
            Debug.LogError("Jump");
            SoundManager.S.PlaySound(SoundType.PlayerJump);
            Jump gj = other.GetComponent<Jump>();
            this.jumpGravity = this.tempJumpGravity = gj.jumpGravity;
            this.jumpFallSpeed = gj.jumpFallSpeed;
            inJump = true;
            curTrunValue = 0;
            this.body.SetLocalEulerX(-20);
            //cameraCtl.cameraFollowSpeed = 60;
            //this.accTimer = 1;

            //if (getJump)
            //{
            //    cameraCtl.SetJump();
            //    this.accTimer = 2f;
            //    this.moveSpeed = 40;
            //}

            //cameraCtl.SetJump();
            //Timer.S.DelayCall(0.5f, () =>
            //{
            //    cameraCtl.SetDown();
            //}, this.gameObject);
        }

        if (other.tag == "Flip")
        {
            Debug.LogError("Flip");
            inFlip = true;
            SoundManager.S.PlaySound(SoundType.PlayerJump);
            Jump gj = other.GetComponent<Jump>();
            this.jumpGravity = this.tempJumpGravity = gj.jumpGravity;
            this.jumpFallSpeed = gj.jumpFallSpeed;
            curTrunValue = 0;
            this.body.SetLocalEulerX(0);
        }


        if ((other.tag == "Road" || other.tag == "Mount") && !isFinish)
        {
            if (getJump)
            {
                ainm.SetBool("Accelerate", false);
                this.jumpTrackTrans.Clear();
                this.moveSpeed = 30;
                //cameraCtl.cameraFollowSpeed = 60;
                this.accTimer = 2;
                //cameraCtl.SetIdle();
                //cameraCtl.idlePos.localPosition = Vector3.Lerp(cameraCtl.idlePos.localPosition, cameraCtl.idlePos_Standard.localPosition, Time.fixedDeltaTime * 10);
                //cameraCtl.idlePos.localRotation = Quaternion.Lerp(cameraCtl.idlePos.localRotation, cameraCtl.idlePos_Standard.localRotation, Time.fixedDeltaTime * 10);
                cameraCtl.idlePos.localPosition = cameraCtl.idlePos_Standard.localPosition;
                cameraCtl.idlePos.localRotation = cameraCtl.idlePos_Standard.localRotation;
                //搜索前方路径点
                for (int i = curWayIndex; i < wayPoint.childCount; i++)
                {
                    Transform tar = WaypointScript.S.points[i];
                    Vector3 dir = (tar.position - this.transform.position).normalized;
                    float dot = Vector3.Dot(dir, transform.forward);
                    //Debug.LogError(i+"    "+dot);
                    if (dot > 0)
                    {
                        curWayIndex = i + 1;
                        Debug.LogError("找到的Index: " + i);
                        this.getJump = false;
                        break;
                    }
                }
            }
            if (inJump)
            {
                SoundManager.S.DoBuzz(BuzzType.Hit);
                SoundManager.S.PlaySound(SoundType.PlayerLand);
                PlayFireFx();
                DOVirtual.DelayedCall(1, () => { PlayFireFx(false); });
                cameraCtl.SetIdle();
                cameraCtl.ShakeOnce2();
                inJump = false;
                tempJumpGravity = jumpGravity;
            }

            if (inFlip)
            {
                SoundManager.S.DoBuzz(BuzzType.Hit);
                SoundManager.S.PlaySound(SoundType.PlayerLand);
                PlayFireFx();
                DOVirtual.DelayedCall(1, () => { PlayFireFx(false); });
                cameraCtl.idlePos.localPosition = cameraCtl.idlePos_Standard.localPosition;
                cameraCtl.idlePos.localRotation = cameraCtl.idlePos_Standard.localRotation;
                this.body.localEulerAngles = Vector3.zero;
                cameraCtl.SetIdle();
                cameraCtl.ShakeOnce2();
                inFlip = false;
                tempJumpGravity = jumpGravity;
            }
        }

        if (other.tag == "Enemy")
        {
            other.SendMessageUpwards("PlayDieSound", SendMessageOptions.DontRequireReceiver);
            cameraCtl.ShakeOnce3();
            //DOVirtual.DelayedCall(0.1f, () =>
            //{
            //    float ttt = 1f;
            //    DOTween.To(() => ttt, x => ttt = x, 0f, 0).SetUpdate(true).OnUpdate(() =>
            //    {
            //        Time.timeScale = ttt;
            //        //Debug.LogError(ttt);
            //    }).SetEase(Ease.OutQuint).OnComplete(() =>
            //    {
            //        DOVirtual.DelayedCall(0.05f, () =>
            //        {
            //            Time.timeScale = 1;
            //            //Debug.LogError("OnComplete");
            //        });
            //    });

            //});
            this.moveSpeed = 60;
            maxMoveSpeed = 60;
            this.accelerateTimer = this.accelerateInterval = 1f;
            this.accTimer = 1;
            cameraCtl.blur.blurScale = 0.007f;
            PlayAccFx();
            foreach (Renderer ren in allRenders)
            {
                ren.material.SetFloat("_RimLerp", 1);
            }
            inAccelerate = true;


            SoundManager.S.DoBuzz(BuzzType.Hit);
            SoundManager.S.PlaySound(SoundType.PlayerHit);
        }

        if (other.tag == "Accelerate")
        {
            SoundManager.S.PlaySound(SoundType.PlayerSpeedup);
            Accelerate acc = other.GetComponent<Accelerate>();
            this.moveSpeed = acc.accSpeed;
            maxMoveSpeed = acc.accSpeed;
            this.accelerateTimer = this.accelerateInterval = acc.accTime;
            this.accTimer = 1;
            cameraCtl.blur.blurScale = 0.007f;
            PlayAccFx();
            foreach (Renderer ren in allRenders)
            {
                ren.material.SetFloat("_RimLerp", 1);
            }
            ainm.SetBool("Accelerate", true);
            inAccelerate = true;
        }

        if (other.tag == "StopSpawnEnemy")
        {
            EnemySpawner.S.canSpawn = false;
            EventSystem.S.Send(EventID.OnStopSpawnEnemy);
        }

        if (other.tag == "Weather")
        {
            cameraCtl.CloseAllWeather();
        }
        if (other.tag == "ScoreBoard")
        {
            if (getScoreBoard) return;
            SoundManager.S.DoBuzz(BuzzType.Win);
            SoundManager.S.PlaySound(SoundType.PlayerLand);
            this.lrSpeed = 20;
            cameraCtl.SetFinish();
            cameraCtl.ShakeOnce2();
            inJump = false;
            tempJumpGravity = jumpGravity;
            cameraCtl.OpenWeather(false);
            ainm.SetBool("Standup", true);
            this.getScoreBoard = true;
            Timer.S.DelayCall(1.6f, () =>
            {
                startMove = false;
                int _id = Random.Range(0, scoreList.Count);
                UIManager.S.curLevelWinMoneyMulti = scoreList[_id].score;
            }, this.gameObject);
            Timer.S.DelayCall(2.2f, () =>
            {
                SoundManager.S.PlaySound(SoundType.PlayerFinish);
                cameraCtl.SetScore();
                UIManager.S.Change2WinPanel();
            }, this.gameObject);

        }

        if (other.tag == "Finish")
        {
            isFinish = true;
        }
        if (other.tag == "Score")
        {
            Score sc = other.GetComponent<Score>();
            scoreList.Add(sc);
        }

        if (other.tag == "CarSide" || other.tag == "Stone")
        {
            DoDie();
        }

        if (other.tag == "Mount")
        {
            inSnow = true;
        }

        if (other.tag == "Coin")
        {
            UIManager.S.curLevelWinMoneyNum += 10;
        }

        //if (other.tag == "HitItem")
        //{
        //    other.SendMessage("OnHit", transform.position);
        //}
    }
    public List<Score> scoreList = new List<Score>();
    public void TriggerExit(Collider other)
    {
        if (isDie) return;
        if (other.tag == "JumpArea")
        {
            this.getInJumpArea = false;
        }
        if (other.tag == "Car")
        {
            inCar = false;
            cameraCtl.SetIdle();
        }
        if (other.tag == "Score")
        {
            Score sc = other.GetComponent<Score>();
            scoreList.Remove(sc);
        }
        if (other.tag == "Mount")
        {
            inSnow = false;
        }
    }

    private void DoDie()
    {
        if (isDie) return;
        SoundManager.S.DoBuzz(BuzzType.Die);
        SoundManager.S.PlaySound(SoundType.PlayerDie);
        SoundManager.S.PlayRollSound(0, SoundType.PlayerRollRoad);
        this.isDie = true;
        this.PlayAccFx(false);
        this.PlayFireFx(false);
        this.PlayMountFx(0, false);
        this.PlayMountFx(1, false);
        cameraCtl.blur.blurScale = 0;
        cameraCtl.OpenWeather(false);
        cameraCtl.SetDie();
        foreach (Renderer ren in allRenders)
        {
            ren.material.SetFloat("_RimLerp", 0);
        }
        this.ainm.enabled = false;
        this.cc.enabled = false;
        foreach (Rigidbody rb in this.body.GetComponentsInChildren<Rigidbody>())
        {
            if (!rb.transform.name.Contains("Dummy"))
            {
                rb.isKinematic = false;
                rb.GetComponent<Collider>().isTrigger = false;
                rb.AddExplosionForce(Random.Range(1000, 2000), explosionPos.position, Random.Range(10, 40), Random.Range(0.1f, 0.15f));
            }
        }
        Timer.S.DelayCall(2, () => { UIManager.S.Change2FailPanel(); }, this.gameObject);
    }
    public void TriggerSnow(int id, bool flag)
    {
        PlayMountFx(id, flag);
    }
    private void PlayMountFx(int id, bool flag = false)
    {
        if (isDie) flag = false;
        if (flag)
        {
            this.snowFx[id].Play();
        }
        else
        {
            this.snowFx[id].Stop();
        }
    }
    private void PlayAccFx(bool flag = true)
    {
        if (isDie) flag = false;
        foreach (ParticleSystem fx in accFx)
        {
            if (flag)
            {
                fx.Play(true);
            }
            else
            {
                fx.Stop(true);
            }
        }
    }

    private void PlayFireFx(bool flag = true)
    {
        if (isDie) flag = false;
        foreach (ParticleSystem fx in fireFx)
        {
            if (flag)
            {
                fx.Play(true);
            }
            else
            {
                fx.Stop(true);
            }
        }
    }
    public void StartMove()
    {
        UIManager.S.Change2GamePanel();
        cameraCtl.startFollow = true;
        //this.SetDir(Vector2.up);
        cameraCtl.cameraFollow.position = cameraCtl.idlePos.position;
        cameraCtl.cameraFollow.rotation = cameraCtl.idlePos.rotation;
        cameraCtl.SetIdle();
        this.startMove = true;
        cameraCtl.StartShake();
        EnemySpawner.S.canSpawn = true;
        EventSystem.S.Send(EventID.OnGameStart);

        Timer.S.DelayCall(0.5f, () =>
        {
            cameraCtl.OpenWeather();
        }, this.gameObject);
    }

    public void SetSpeedType(SpeedType type)
    {
        this.speedType = type;
    }

    public void SetDir(Vector2 dir)
    {
        //int thex = 0;
        //if (dir.x > 0)
        //{
        //    thex = 1;
        //}
        //else if (dir.x < 0)
        //{
        //    thex = -1;
        //}
        //this.moveDir = new Vector3(thex, 0, 1);
        //Debug.LogError(dir.x);
        this.moveDir = new Vector3(dir.x, 0, 1);
        //this.DoRotate();
    }

    //private void DoRotate()
    //{
    //    //this.transform.eulerAngles += new Vector3(0, this.moveDir.x * this.rotateSpeed * Time.fixedDeltaTime, 0);
    //}

    //public void UpdateCamera(Vector2 dir)
    //{
    //    // this.cameraCtl.transform.rotation = Quaternion.Lerp(this.cameraCtl.transform.rotation, Quaternion.Euler(cameraCtl.transform.eulerAngles.x, cameraCtl.transform.eulerAngles.y, dir.x * 10), 1);
    //}
}
