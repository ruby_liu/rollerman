﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollider : MonoBehaviour
{
    public Enemy enemy;
    void OnTriggerEnter(Collider other)
    {
        enemy.TriggerEnter(other);
    }
}
