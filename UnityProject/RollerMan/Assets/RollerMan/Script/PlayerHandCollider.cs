﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandCollider : MonoBehaviour
{
    public PlayerSystem player;
    public int id = 0; //0是左手1是右手
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Mount")
            player.TriggerSnow(id, true);
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Mount")
            player.TriggerSnow(id, false);
    }
}
