﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Timer = MidnightCoder.Game.Timer;

public class Car : MonoBehaviour
{
    public Transform body;
    public CharacterController cc;
    //public Rigidbody rb;
    public float moveSpeed = 3;
    public float maxMoveSpeed = 3;
    public Vector3 moveDir;
    public Vector3 desDir;
    public bool startMove = false;
    public float gravity = 2;

    public int curWayIndex = -1;
    public bool isDie = false;

    private float dieTimer = 3;
    private void Awake()
    {

    }

    void Update()
    {
        if (Vector3.Dot(PlayerSystem.S.transform.forward, (this.transform.position - PlayerSystem.S.transform.position).normalized) < 0)
        {
            dieTimer -= Time.deltaTime;
            if (dieTimer < 0)
            {
                dieTimer = 1000;
                Destroy(this.gameObject);
            }
        }
    }
    public Transform rayPoint;

    void FixedUpdate()
    {
        if (this.startMove)
        {
            if (curWayIndex < EnemySpawner.S.wayPoint.childCount && curWayIndex >= 0)
            {
                Transform targetPoint = WaypointScript.S.points[curWayIndex];
                Vector3 dir = targetPoint.position - transform.position;
                float dotValue = Vector3.Dot(transform.forward, dir);
                float angleValue = Vector3.Angle(transform.forward, targetPoint.right);
                if (dotValue > 1)
                {
                    if (angleValue > 0)
                    {
                        //transform.forward = Vector3.Lerp(transform.forward, -targetPoint.right, Time.fixedDeltaTime * 1.1f);
                        //body.SetEulerZ(-targetPoint.eulerAngles.z);//= Quaternion.Lerp(body.rotation, targetPoint.rotation, Time.fixedDeltaTime);

                        Transform nextP = targetPoint;
                        Quaternion nextRot2 = Quaternion.LookRotation(Vector3.Cross(nextP.forward, Vector3.Cross(transform.forward, nextP.forward)), nextP.forward);
                        transform.rotation = Quaternion.Lerp(transform.rotation, nextRot2, Time.fixedDeltaTime * 5f);

                        Quaternion nextRot3 = Quaternion.LookRotation(-nextP.right, nextP.forward);
                        transform.rotation = Quaternion.Lerp(transform.rotation, nextRot3, Time.fixedDeltaTime *10);


                        if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
                            1 << LayerMask.NameToLayer("Road")))
                        {
                            //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
                            //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
                            Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(body.forward, hit.normal)), hit.normal);
                            body.rotation = Quaternion.Lerp(body.rotation, nextRot, 0.1f);

                        }
                    }
                }
                else if (dotValue <= 1)
                {
                    curWayIndex--;
                }
            }
            //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 0, Time.fixedDeltaTime * 50));
            desDir = (transform.forward * moveSpeed );

            cc.Move(desDir * Time.fixedDeltaTime);
            this.moveSpeed = Mathf.Clamp(moveSpeed + Time.fixedDeltaTime * 10, 0, maxMoveSpeed);
        }

    }


    public void StartMove(int key, params object[] param)
    {
        this.startMove = true;
    }

}
