﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;
using MoreMountains.NiceVibrations;

public enum SoundType
{
    PlayerDie,
    PlayerHit,
    PlayerJump,
    PlayerLand,
    PlayerSpeedup,
    PlayerFinish,
    PlayerRollRoad,
    PlayerRollSnow,
    PlayerRollFly
}

public enum BuzzType
{
    Hit,
    Die,
    Win
}
public class SoundManager : TMonoSingleton<SoundManager>
{
    public AudioSource bgmSource;
    public AudioSource soundSource;
    public AudioSource soundSource_roll;
    public AudioClip bgmMain;
    public AudioClip playerDieClip;
    public AudioClip playerHitClip;
    public AudioClip playerJumpClip;
    public AudioClip playerLandClip;
    public AudioClip playerSpeedupClip;
    public AudioClip playerFinishClip;
    public AudioClip playerRollRoadClip;
    public AudioClip playerRollSnowClip;
    public AudioClip playerRollFlyClip;

    public SoundType playerRollType;

    public void PlayBgmMain()
    {
        if (UIManager.S.soundOn)
        {
            bgmSource.clip = bgmMain;
            bgmSource.Play();
        }
        else
        {
            bgmSource.Stop();
        }
    }

    public void UpdateBgmSourceOn()
    {
        bgmSource.volume = UIManager.S.soundOn ? 1 : 0;
        if (!bgmSource.isPlaying)
        {
            bgmSource.clip = bgmMain;
            bgmSource.Play();
        }
    }
    public void PlaySound(SoundType type)
    {
        if (!UIManager.S.soundOn) return;
        switch (type)
        {
            case SoundType.PlayerDie:
                soundSource.PlayOneShot(playerDieClip); break;
            case SoundType.PlayerHit:
                soundSource.PlayOneShot(playerHitClip); break;
            case SoundType.PlayerJump:
                soundSource.PlayOneShot(playerJumpClip); break;
            case SoundType.PlayerLand:
                soundSource.PlayOneShot(playerLandClip); break;
            case SoundType.PlayerSpeedup:
                soundSource.PlayOneShot(playerSpeedupClip); break;
            case SoundType.PlayerFinish:
                soundSource.PlayOneShot(playerFinishClip); break;
        }
    }

    public void PlayRollSound(float volume,SoundType type)
    {
        if (!UIManager.S.soundOn) return;
        if (volume != 0)
        {
            if (playerRollType != type)
            {
                playerRollType = type;
                switch (type)
                {
                    case SoundType.PlayerRollRoad:
                        soundSource_roll.clip = playerRollRoadClip; break;
                    case SoundType.PlayerRollSnow:
                        soundSource_roll.clip = playerRollSnowClip; break;
                    case SoundType.PlayerRollFly:
                        soundSource_roll.clip = playerRollFlyClip; break;
                }
            }

            if (!this.soundSource_roll.isPlaying)
            {
                this.soundSource_roll.Play();
            }
        }
       
        this.soundSource_roll.volume = volume;
    }

    public void DoBuzz(BuzzType type)
    {
        if (!UIManager.S.buzzOn) return;
        switch (type)
        {
            case BuzzType.Hit: MMVibrationManager.Haptic(HapticTypes.MediumImpact); break;
            case BuzzType.Die: MMVibrationManager.Haptic(HapticTypes.Failure); break;
            case BuzzType.Win: MMVibrationManager.Haptic(HapticTypes.Success); break;
        }
    }
}
