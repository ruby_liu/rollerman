﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class TestWayPointSpawner : MonoBehaviour
{
    public Transform wayPointParent;
    [ContextMenu("生成路径点")]
    public void SpawnPoint()
    {
        GameObject last = this.transform.GetChild(this.transform.childCount - 1).gameObject;
        GameObject obj = Instantiate(last, this.transform);
        obj.transform.position = last.transform.position + last.transform.forward * 80;
        obj.name = "P" + this.transform.childCount;
    }

    [ContextMenu("路径点全部落地")]
    public void MakePointDownOnFloor()
    {
        int len = transform.childCount;
        int baseId = int.Parse(transform.GetChild(0).name.Split('P')[1]);
        for (int i = 0; i < len; i++)
        {
            Transform item = transform.GetChild(i);
            if (Physics.Raycast(new Ray(item.position, Vector3.down), out RaycastHit hit, 1000, 1 << LayerMask.NameToLayer("Road")))
            {
                item.position = hit.point + item.up*0.555f;
                item.up = hit.normal;
            }
            item.name = "P" + (baseId + i);
        }
    }

    [ContextMenu("加速板落地")]
    public void MakeAccFownFloor()
    {
        Transform item = transform;
        if (Physics.Raycast(new Ray(item.position, Vector3.down), out RaycastHit hit, 1000, 1 << LayerMask.NameToLayer("Road")))
        {
            item.position = hit.point + item.up * 0.555f;
            item.forward = hit.normal;
        }
    }
    [ContextMenu("旋转")]
    public void Rota()
    {
        int len = transform.childCount;
        int baseId = 1;
        for (int i = 0; i < len; i++)
        {
            Transform item = transform.GetChild(i);
            item.SetLocalEulerZ(item.localEulerAngles.z + 180);
            item.name = "P" + (baseId + i);
        }
    }


}
