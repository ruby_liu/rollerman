﻿using System.Collections;
using System.Collections.Generic;
using MidnightCoder.Game;
using UnityEngine;

public class Coin : MonoBehaviour
{
    //public GameObject coinFx;
    public MeshRenderer render;

    private void Update()
    {
        transform.Rotate(Vector3.forward, 5);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //this.coinFx.SetActive(true);
            //this.render.enabled = false;
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
            //Timer.S.DelayCall(1, () => { Destroy(this.gameObject); }, this.gameObject);
        }
    }

    [ContextMenu("落地")]
    public void Land()
    {
        if (Physics.Raycast(new Ray(transform.position, Vector3.down), out RaycastHit hit, 1000, 1 << LayerMask.NameToLayer("Road")))
        {
            transform.position = hit.point + Vector3.up ;
        }
    }

}
