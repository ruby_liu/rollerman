﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MidnightCoder.Game;

public enum EnemyType
{
    Blue = 0,
    Black,
    Green,
    Orange,
    Pink,
    Purple,
    Yellow,
    None
}
public class EnemySpawner : TMonoSingleton<EnemySpawner>
{
    public Transform enemyRoot;
    public Material[] allMats;
    public Sprite[] allSprites;
    public GameObject[] enemyPrefabs;
    //public GameObject[] carPrefabs;

    public PlayerSystem player;
    public Transform wayPoint;

    //public List<Enemy> enemyList;

    private float spawnInterval_Enemy = 2;
    private float spawnTimer_Enemy = 2;

    //private float spawnInterval_Car = 20;
    //private float spawnTimer_Car = 10;

    public int playerCurIndex = 0;

    public bool canSpawn = false;

    private int idSort = 1;
    public List<int> enemyFrontPlayer;

    public int allBeHitNum = 0;
    private void Awake()
    {
        //enemyList = new List<Enemy>();
        enemyFrontPlayer = new List<int>();
    }
    void Start()
    {
        foreach (Transform e in enemyRoot)
        {
            Enemy ec = e.GetComponent<Enemy>();
            ec.id = idSort;
            idSort++;
        }
    }

    void FixedUpdate()
    {
        if (!canSpawn) return;
        this.spawnTimer_Enemy -= Time.deltaTime;
        if (spawnTimer_Enemy < 0)
        {
            spawnTimer_Enemy = this.spawnInterval_Enemy;
            this.SpawnEnemy();
        }
    }

    public void AddEnemyFrontPlayer(int id)
    {
        if (enemyFrontPlayer.Contains(id)) return;
        enemyFrontPlayer.Add(id);
    }
    public void RemoveEnemyFrontPlayer(int id)
    {
        this.enemyFrontPlayer.Remove(id);
    }
    void SpawnEnemy()
    {
        int id = PlayerSystem.S.getJump ? Random.Range(100, 150) : Random.Range(20, 40);
        int num = PlayerSystem.S.getJump ? Random.Range(5, 8) : Random.Range(3, 6);
        playerCurIndex = player.curWayIndex;
        if (playerCurIndex < this.wayPoint.childCount - id)
        {
            for (int i = 0; i < num; i++)
            {
                GameObject obj = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)], enemyRoot);
                Enemy e = obj.GetComponent<Enemy>();
                e.id = idSort++;
                //enemyList.Add(e);
                int eIndex = playerCurIndex + id;
                e.curWayIndex = eIndex;
                //Vector3 beforePoint = this.wayPoint.GetChild(eIndex).position;
                //if(eIndex -1 > 0)
                //{
                //    beforePoint = this.wayPoint.GetChild(eIndex - 1).position;
                //}
                //if (Vector3.Distance(this.wayPoint.GetChild(eIndex).position, beforePoint) > 200)
                //{
                //    e.transform.position = player.transform.position + player.transform.forward * 80* (2+id);
                //}
                //    else
                //   {
                e.transform.position = WaypointScript.S.points[eIndex].position;
                //    }

                e.transform.position -= e.transform.forward.normalized * Random.Range(5, 25);
                e.transform.position += e.transform.right.normalized * Random.Range(-10, 11);
                e.StartMove();
            }
        }
    }

    //public  void SpawnCar()
    //{
    //    int id = Random.Range(50, 80);
    //    int num = 1;
    //    playerCurIndex = player.curWayIndex;
    //    if (playerCurIndex < this.wayPoint.childCount - id)
    //    {
    //        for (int i = 0; i < num; i++)
    //        {
    //            GameObject obj = Instantiate(carPrefabs[Random.Range(0, enemyPrefabs.Length)]);
    //            Car e = obj.GetComponent<Car>();
    //            //enemyList.Add(e);
    //            int eIndex = playerCurIndex + id - 1;
    //            e.curWayIndex = eIndex;
    //            Transform tar = WaypointScript.S.points[eIndex+1];
    //            e.transform.position = tar.position;
    //            //e.transform.localPosition += e.transform.forward.normalized * Random.Range(5, 25);
    //            e.transform.eulerAngles = tar.eulerAngles;
    //            e.transform.rotation = tar.rotation;
    //            e.transform.LookAt(WaypointScript.S.points[eIndex]);
    //            //e.transform.Rotate(e.transform.up,e.transform.localEulerAngles.y+180,Space.Self);
    //            //  e.transform.SetLocalEulerY(e.transform.localEulerAngles.y + 180);
    //            e.StartMove(0);
    //        }
    //    }
    //}
}


