﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFx : MonoBehaviour
{
    public GameObject fx;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            this.fx.SetActive(true);
            PlayerSystem.S.cameraCtl.OpenWeather();
        }
    }
}
