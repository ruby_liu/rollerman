﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MidnightCoder.Game;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class UIManager : TMonoSingleton<UIManager>
{
    public Transform mainPanel;
    public Transform readyTrans;
    public Transform gameTrans;
    public Transform winPanel;
    public Transform failPanel;

    public Button buzzBtn;
    public Button soundBtn;
    public bool buzzOn = true;
    public bool soundOn = true;

    public Image gameRankIcon;
    public Text gameRankText;
    public Sprite[] rankIcons;
    public Color rankColorLow;
    public Color rankColorHigh;
    public Image speedProgress;
    public Transform killTransform;
    public Text killNumText;

    public Text winAllMoneyText;
    public Image winRankIcon;
    public Text winRankText;
    public Text winHitText;
    public Text winGetMoneyText;
    public Text winClaimText;
    public Button winClaimBtn;
    public Button winLosebtn;
    public GameObject winMoneyFly;
    public GameObject winMoneyFlyOrigin;


    public Button loseReviveBtn;
    public Button loseAgainBtn;
    public Text loseAllMoneyText;


    private float speedProgressValue;
    private int gameRankNum = -1;
    private string[] rearStrArr;
    private string rearStr = "";

    public int curLevelWinMoneyNum = 0;
    public int allMoneyNum = 0;
    public int curLevelWinMoneyMulti = 1;


    void Awake()
    {
        rearStrArr = new[] { "st", "nd", "rd", "th" };

        if (PlayerPrefs.HasKey("AllMoney"))
        {
            allMoneyNum = PlayerPrefs.GetInt("AllMoney");
        }
        if (PlayerPrefs.HasKey("Buzz"))
        {
            buzzOn = PlayerPrefs.GetInt("Buzz") == 1;
        }
        if (PlayerPrefs.HasKey("Sound"))
        {
            soundOn = PlayerPrefs.GetInt("Sound") == 1;
        }
        //Debug.LogError(soundOn);

        this.buzzBtn.onClick.AddListener(this.OnBuzzBtnClick);
        this.soundBtn.onClick.AddListener(this.OnSoundBtnClick);
        this.winLosebtn.onClick.AddListener(this.OnLoseBtnClick);
        this.winClaimBtn.onClick.AddListener(this.OnClaimBtnClick);
        this.loseReviveBtn.onClick.AddListener(this.OnReviveBtnClick);
        this.loseAgainBtn.onClick.AddListener(this.OnAgainBtnClick);

        UpdateSettingAndShow();
    }
    void Start()
    {
        SoundManager.S.PlayBgmMain();
        //Debug.LogError(soundOn);

    }

    void Update()
    {
        this.speedProgress.fillAmount = Mathf.Lerp(this.speedProgress.fillAmount, speedProgressValue, Time.deltaTime * 2);

        if (gameRankNum > 0)
        {
            string str = rearStr;
            str = gameRankNum + rearStr;
            gameRankText.text = str;
        }

    }

    public void Change2GamePanel()
    {
        mainPanel.gameObject.SetActive(true);
        readyTrans.gameObject.SetActive(false);
        gameTrans.gameObject.SetActive(true);
        winPanel.gameObject.SetActive(false);
    }


    public void Change2WinPanel()
    {
        mainPanel.gameObject.SetActive(false);
        readyTrans.gameObject.SetActive(false);
        gameTrans.gameObject.SetActive(false);
        UpdateWinPanelShow();
        winPanel.gameObject.SetActive(true);
    }
    public void Change2FailPanel()
    {
        mainPanel.gameObject.SetActive(false);
        readyTrans.gameObject.SetActive(false);
        gameTrans.gameObject.SetActive(false);
        winPanel.gameObject.SetActive(false);
        UpdateLosePanelShow();
        failPanel.gameObject.SetActive(true);
    }
    public void UpdateSpeedProgressValue(float _t)
    {
        speedProgressValue = _t;
    }


    public void UpdateGameRankShow(int _rankNum)
    {
        this.gameRankNum = _rankNum;
        if (_rankNum <= 3)
        {
            this.gameRankIcon.sprite = rankIcons[_rankNum - 1];
            this.gameRankIcon.SetNativeSize();
            this.gameRankText.color = rankColorHigh;
            rearStr = rearStrArr[_rankNum - 1];
        }
        else
        {
            this.gameRankIcon.sprite = rankIcons[3];
            this.gameRankIcon.SetNativeSize();
            this.gameRankText.color = rankColorLow;
            rearStr = rearStrArr[3];
        }
    }

    private void UpdateWinPanelShow()
    {
        if (gameRankNum <= 3)
        {
            this.winRankIcon.sprite = rankIcons[gameRankNum - 1];
            this.winRankIcon.SetNativeSize();
            rearStr = rearStrArr[gameRankNum - 1];
        }
        else
        {
            this.winRankIcon.sprite = rankIcons[3];
            this.winRankIcon.SetNativeSize();
            rearStr = rearStrArr[3];
        }
        winRankText.text = gameRankNum + rearStr;
        winHitText.text = EnemySpawner.S.allBeHitNum.ToString();

        winAllMoneyText.text = allMoneyNum.ToString();
        winGetMoneyText.text = curLevelWinMoneyNum.ToString();
        winClaimText.text = "X" + this.curLevelWinMoneyMulti;
    }

    private void UpdateLosePanelShow()
    {
        loseAllMoneyText.text = allMoneyNum.ToString();
    }

    public void OnBuzzBtnClick()
    {
        this.buzzOn = !buzzOn;
        UpdateSettingAndShow();
        Save();
    }
    public void OnSoundBtnClick()
    {
        this.soundOn = !soundOn;
        UpdateSettingAndShow();
        Save();
        Debug.LogError(soundOn);

    }

    public void UpdateSettingAndShow()
    {
        buzzBtn.transform.GetChild(0).gameObject.SetActive(!buzzOn);
        buzzBtn.transform.GetChild(1).gameObject.SetActive(buzzOn);
        soundBtn.transform.GetChild(0).gameObject.SetActive(!soundOn);
        soundBtn.transform.GetChild(1).gameObject.SetActive(soundOn);
        SoundManager.S.UpdateBgmSourceOn();
    }
    public void OnClaimBtnClick()
    {
        //AddMoneyNum(this.curLevelWinMoneyNum * curLevelWinMoneyMulti);

        this.winClaimBtn.interactable = false;
        this.winLosebtn.interactable = false;
        for (int i = 0; i < 30; i++)
        {
            this.FlyDimond();
        }

        this.TweenDimondValue(this.allMoneyNum + this.curLevelWinMoneyNum * curLevelWinMoneyMulti);

    }

    public void OnLoseBtnClick()
    {
        //AddMoneyNum(this.curLevelWinMoneyNum);
        this.winClaimBtn.interactable = false;
        this.winLosebtn.interactable = false;
        for (int i = 0; i < 20; i++)
        {
            this.FlyDimond();
        }
        this.TweenDimondValue(this.allMoneyNum + this.curLevelWinMoneyNum * curLevelWinMoneyMulti);
    }

    public void OnReviveBtnClick()
    {
        OnAgainBtnClick();
    }

    public void OnAgainBtnClick()
    {
        this.loseAgainBtn.interactable = false;
        this.loseReviveBtn.interactable = false;
        SceneManager.LoadSceneAsync("LoadScene");
    }

    private void FlyDimond()
    {
        GameObject obj = Instantiate(this.winMoneyFly, this.transform);
        RectTransform trans = obj.GetComponent<RectTransform>();
        trans.position = winMoneyFlyOrigin.transform.position;
        trans.localScale = Vector3.zero;
        trans.DOScale(1, 0.3f).SetEase(Ease.OutBack);
        trans.DOLocalMove(Random.insideUnitCircle * 200, Random.Range(0.4f, 0.5f)).SetEase(Ease.OutSine).OnComplete(
            () =>
            {
                trans.DOMove(this.winMoneyFly.transform.position, Random.Range(0.5f, 1.0f)).SetEase(Ease.OutQuart)
                    .SetDelay(Random.Range(0.10f, 0.20f)).OnComplete(() => { trans.gameObject.SetActive(false); });
            });
    }

    private void TweenDimondValue(int maxNum, Action _call = null)
    {
        int curNum = this.allMoneyNum;
        this.allMoneyNum = maxNum;
        Save();
        DOTween.To(() => curNum, x => curNum = x, maxNum, 0.6f).SetDelay(this.curLevelWinMoneyNum * curLevelWinMoneyMulti == 0 ? 0 : 1f)
            .OnUpdate(() => { this.winAllMoneyText.text = curNum.ToString(); }).OnComplete(() =>
            {
                //  _call?.Invoke();
                DOVirtual.DelayedCall(0.2f, () => { SceneManager.LoadSceneAsync("LoadScene"); });
            });
    }

    private int killTimerId = -1;
    public void UpdateKillNumShow(int num)
    {
        if (killTimerId >= 0)
        {
            Timer.S.Stop(killTimerId);
            killTimerId = -1;
        }
        this.killNumText.text = num.ToString();
        this.killTransform.gameObject.SetActive(true);
        killTimerId = Timer.S.DelayCall(1.5f, () =>
        {
            this.killTransform.gameObject.SetActive(false);
        }, this.gameObject);
    }


    public void AddMoneyNum(int num)
    {
        this.allMoneyNum += num;
        Save();
    }

    public void Save()
    {
        PlayerPrefs.SetInt("AllMoney", allMoneyNum);
        PlayerPrefs.SetInt("Buzz", buzzOn ? 1 : 2);
        PlayerPrefs.SetInt("Sound", soundOn ? 1 : 2);

    }
}
