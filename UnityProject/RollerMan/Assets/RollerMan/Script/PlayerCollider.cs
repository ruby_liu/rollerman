﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    public PlayerSystem player;
    void OnTriggerEnter(Collider other)
    {
        player.TriggerEnter(other);
    }
    void OnTriggerExit(Collider other)
    {
        player.TriggerExit(other);
    }
}
