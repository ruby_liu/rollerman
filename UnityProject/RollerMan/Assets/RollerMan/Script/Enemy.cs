﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MidnightCoder.Game;
using UnityEngine;
using Timer = MidnightCoder.Game.Timer;

public class Enemy : MonoBehaviour
{
    public int id = -1;
    public EnemyType enemyType = EnemyType.None;
    public Collider normalColli;
    public Collider dieColli;
    public Renderer[] renders;
    public Transform typeFlag;
    public Transform head;
    public Transform body;
    public CharacterController cc;
    public Animator anim;
    public AudioSource au;
    public AudioClip[] dieClips;
    public float moveSpeed = 3;
    public float maxMoveSpeed = 3;
    public float rotateSpeed = 3;
    public float lrSpeed = 1;
    public Vector3 moveDir;
    public Vector3 desDir;
    public bool startMove = false;
    public float gravity = 2;
    public float jumpGravity = -40;
    private float tempJumpGravity = -40;
    private int jumpFallSpeed = 50;
    private Queue<Transform> jumpTrackTrans;

    public bool getJump = false;
    private bool inJump = false;

    public int curWayIndex = -1;
    public float changeDirInterval = 2;
    private float changeDirTimer = 3;


    public bool isDie = false;
    private float dieRoty = 0;
    private float dieRotz = 0;

    private int maxRandSpeed = 80;

    private bool isStopSpawn = false;
    private float curTrunValue = 0;

    private bool getScoreBoard = false;
    private bool isFinish = false;

    private float dieTimer = 6;

    private void Awake()
    {
        EventSystem.S.Register(EventID.OnGameStart, EventStartMove);
        EventSystem.S.Register(EventID.OnStopSpawnEnemy, EventStopSpawnEnemy);
        jumpTrackTrans = new Queue<Transform>();
    }

    void Start()
    {
        EnemyType t;
        EnemyType.TryParse(Random.Range(0, 7).ToString(), out t);
        SetType(t);
    }
    private void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnGameStart, EventStartMove);
        EventSystem.S.UnRegister(EventID.OnStopSpawnEnemy, EventStopSpawnEnemy);
    }
    void Update()
    {
        if (!isDie && !isFinish)
        {
            if (IsWorldFrontPlayer())
            {
                EnemySpawner.S.AddEnemyFrontPlayer(this.id);
            }
            else
            {
                EnemySpawner.S.RemoveEnemyFrontPlayer(this.id);
            }
        }
      

        if (this.startMove && !getJump)
        {
            this.changeDirTimer -= Time.deltaTime;
            if (this.changeDirTimer < 0)
            {
                changeDirInterval = Random.Range(1.0f, 3.0f);
                this.changeDirTimer = this.changeDirInterval;
                //  this.SetDir(Random.insideUnitCircle);
                if (IsFrontPlayer())
                {
                    maxRandSpeed = isStopSpawn ? 40 : 40;
                }
                else if (!isStopSpawn)
                {
                    maxRandSpeed = 60;
                }
                this.maxMoveSpeed = Random.Range(20, maxRandSpeed);
            }
        }
        if (isDie)
        {
            //desDir = this.moveDir * moveSpeed + Vector3.down * tempJumpGravity;
            //tempJumpGravity += Time.deltaTime * 70;
            //this.body.SetLocalEulerY(this.body.localEulerAngles.y + dieRoty);
            //this.body.SetLocalEulerZ(this.body.localEulerAngles.z + dieRotz);
            //cc.Move(desDir * Time.deltaTime);

            //foreach (Rigidbody rb in this.body.GetComponentsInChildren<Rigidbody>())
            //{
            //    if (rb.transform.name == "Bip001 Pelvis")
            //    {
            //        rb.AddExplosionForce(1000, , 8);
            //    }
            //}
            dieColli.transform.localPosition = Vector3.zero;
            dieColli.transform.localEulerAngles = Vector3.zero;
            dieTimer -= Time.deltaTime;
            if (dieTimer < 0)
            {
                dieTimer = 6;
                Destroy(this.gameObject);
            }
        }

        typeFlag.position = head.position + Vector3.up;
        typeFlag.LookAt(Camera.main.transform);

    }

    public void SetType(EnemyType type)
    {
        this.enemyType = type;
        this.UpdateEnemyTypeShow();
    }

    public void UpdateEnemyTypeShow()
    {
        if (this.enemyType == EnemyType.None) return;
        foreach (Renderer ren in renders)
        {
            ren.material = EnemySpawner.S.allMats[this.enemyType.GetHashCode()];
        }

        this.typeFlag.GetComponent<SpriteRenderer>().sprite = EnemySpawner.S.allSprites[this.enemyType.GetHashCode()];
    }
    public Transform rayPoint;

    //void FixedUpdate()
    //{
    //    if (this.startMove)
    //    {
    //        if (!getJump)
    //        {
    //            if (curWayIndex < EnemySpawner.S.wayPoint.childCount && curWayIndex >= 0)
    //            {
    //                Transform targetPoint = EnemySpawner.S.wayPoint.GetChild(curWayIndex);
    //                Vector3 dir = targetPoint.position - transform.position;
    //                float dotValue = Vector3.Dot(transform.forward, dir);
    //                float angleValue = Vector3.Angle(transform.forward, targetPoint.forward);
    //                if (dotValue < 10)
    //                {
    //                    if (dotValue > 1)
    //                    {
    //                        if (angleValue > 0)
    //                        {
    //                            if (!inJump)
    //                            {
    //                                //transform.forward = Vector3.Lerp(transform.forward, targetPoint.forward, Time.fixedDeltaTime * 1.2f);
    //                                //body.rotation = Quaternion.Lerp(body.rotation, targetPoint.rotation, Time.fixedDeltaTime);
    //                                Transform nextP = WaypointScript.S.points[curWayIndex];
    //                                Quaternion nextRot2 = Quaternion.LookRotation(Vector3.Cross(nextP.forward, Vector3.Cross(transform.forward, nextP.forward)), nextP.forward);
    //                                transform.rotation = Quaternion.Lerp(transform.rotation, nextRot2, Time.fixedDeltaTime * 5f);

    //                                Quaternion nextRot3 = Quaternion.LookRotation(nextP.right, nextP.forward);
    //                                transform.rotation = Quaternion.Lerp(transform.rotation, nextRot3, Time.fixedDeltaTime * 15);


    //                                if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
    //                                    1 << LayerMask.NameToLayer("Road")))
    //                                {
    //                                    //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
    //                                    //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
    //                                    Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(body.forward, hit.normal)), hit.normal);
    //                                    body.rotation = Quaternion.Lerp(body.rotation, nextRot, 0.1f);

    //                                }
    //                            }
    //                        }
    //                    }
    //                    else if (dotValue <= 1)
    //                    {
    //                        curWayIndex++;
    //                    }
    //                }
    //            }

    //            this.lrSpeed = moveSpeed / 8;
    //            float speedX = moveDir.x * lrSpeed;
    //            if (inJump)
    //            {
    //                if (this.body.localEulerAngles.Equals(Vector3.zero))
    //                {
    //                    this.body.SetLocalEulerX(-20);
    //                }

    //                desDir = (transform.forward * moveSpeed + Vector3.down * tempJumpGravity);
    //                tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;
    //                this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));
    //            }
    //            else
    //            {
    //                this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 0, Time.fixedDeltaTime * 50));
    //                desDir = (transform.forward * moveSpeed + Vector3.down * gravity);
    //            }

    //            desDir.x += speedX;
    //        }
    //        else
    //        {
    //            if (inJump)
    //            {
    //                // desDir = (jumpJudgeForward * moveSpeed + Vector3.down * tempJumpGravity);
    //                // tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;
    //                //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));
    //                if (jumpTrackTrans.Count > 0)
    //                {
    //                    Transform ele = jumpTrackTrans.Peek();
    //                    if (Vector3.Dot(transform.forward, (ele.position - transform.position).normalized) < 0)
    //                    {
    //                        jumpTrackTrans.Dequeue();
    //                    }

    //                    Vector3 forwar = (ele.position - transform.position).normalized;
    //                    desDir = forwar * moveSpeed;
    //                    transform.forward = Vector3.Lerp(transform.forward, forwar, Time.fixedDeltaTime);
    //                }
    //            }
    //        }
    //        cc.Move(desDir * Time.fixedDeltaTime);

    //        this.moveSpeed = Mathf.Clamp(moveSpeed + Time.fixedDeltaTime * 10, 0, maxMoveSpeed);

    //    }


    //}
    void FixedUpdate()
    {
        if (this.startMove)
        {
            if (!getJump)
            {
                if (curWayIndex < EnemySpawner.S.wayPoint.childCount && curWayIndex >= 0)
                {
                    Transform targetPoint = WaypointScript.S.points[curWayIndex];
                    Vector3 dir = targetPoint.position - transform.position;
                    float dotValue = Vector3.Dot(transform.forward, dir);
                    float angleValue = Vector3.Angle(transform.forward, targetPoint.right);
                    if (dotValue < 10)
                    {
                        if (dotValue > 1)
                        {
                            if (angleValue > 0)
                            {
                                if (!inJump && !inFlip)
                                {
                                    //transform.forward = Vector3.Lerp(transform.forward, targetPoint.forward, Time.fixedDeltaTime * 1.2f);
                                    //body.rotation = Quaternion.Lerp(body.rotation, targetPoint.rotation, Time.fixedDeltaTime);
                                    Transform nextP = WaypointScript.S.points[curWayIndex];
                                    Quaternion nextRot2 = Quaternion.LookRotation(Vector3.Cross(nextP.forward, Vector3.Cross(transform.forward, nextP.forward)), nextP.forward);
                                    transform.rotation = Quaternion.Lerp(transform.rotation, nextRot2, Time.fixedDeltaTime * 5f);

                                    Quaternion nextRot3 = Quaternion.LookRotation(nextP.right, nextP.forward);

                                    float a = Quaternion.Angle(transform.rotation, nextRot3);
                                    a = Mathf.Clamp(a / 6, 0, 1);
                                    float y = Vector3.Cross(transform.forward, targetPoint.right).y;
                                    int turnDir = y > 0 ? 1 : -1;
                                    curTrunValue = Mathf.Lerp(curTrunValue, a * turnDir, Time.fixedDeltaTime * 2);
                                    anim.SetFloat("Turn", curTrunValue);

                                    transform.rotation = Quaternion.Lerp(transform.rotation, nextRot3, Time.fixedDeltaTime * 15);

                                    if (Physics.Raycast(rayPoint.position, -rayPoint.up, out RaycastHit hit, 500,
                                        1 << LayerMask.NameToLayer("Road")))
                                    {
                                        //this.body.SetPosY(Mathf.Lerp(this.body.position.y, hit.point.y, Time.deltaTime * 50));
                                        //this.body.position = Vector3.Lerp(this.body.position, hit.point + this.body.up * 0.6f, Time.fixedDeltaTime * 50);
                                        Quaternion nextRot = Quaternion.LookRotation(Vector3.Cross(hit.normal, Vector3.Cross(body.forward, hit.normal)), hit.normal);
                                        body.rotation = Quaternion.Lerp(body.rotation, nextRot, 0.1f);

                                    }
                                }
                            }
                        }
                        else if (dotValue <= 1)
                        {
                            curWayIndex++;
                        }
                    }
                }

                this.lrSpeed = moveSpeed / 8;
                float speedX = moveDir.x * lrSpeed;
                if (inJump)
                {
                    //if (this.body.localEulerAngles.Equals(Vector3.zero))
                    //{
                    //    this.body.SetLocalEulerX(-20);
                    //}

                    desDir = (transform.forward * moveSpeed + Vector3.down * tempJumpGravity);
                    tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;
                    //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));
                    this.body.localRotation = Quaternion.Lerp(this.body.localRotation, Quaternion.Euler(20, this.body.localEulerAngles.y, this.body.localEulerAngles.z), Time.fixedDeltaTime);
                }
                else if (inFlip)
                {
                    anim.SetFloat("Turn", 0);
                    desDir = (transform.forward * moveSpeed + Vector3.down * tempJumpGravity);
                    tempJumpGravity = Mathf.Clamp(tempJumpGravity + Time.fixedDeltaTime * jumpFallSpeed, -100, 30);
                    this.body.Rotate(0, 0, Time.fixedDeltaTime * 400);
                }
                else
                {
                    // this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 0, Time.fixedDeltaTime * 50));
                    desDir = (transform.forward * moveSpeed + Vector3.down * gravity);
                }

                desDir += transform.right * speedX;
            }
            else
            {
                if (inJump)
                {
                    // desDir = (jumpJudgeForward * moveSpeed + Vector3.down * tempJumpGravity);
                    // tempJumpGravity += Time.fixedDeltaTime * jumpFallSpeed;
                    //this.body.SetLocalEulerX(Mathf.Lerp(this.body.localEulerAngles.x, 20, Time.fixedDeltaTime));
                    if (jumpTrackTrans.Count > 0)
                    {
                        Transform ele = jumpTrackTrans.Peek();
                        if (Vector3.Dot(transform.forward, (ele.position - transform.position).normalized) < 0)
                        {
                            jumpTrackTrans.Dequeue();
                        }

                        //Vector3 forwar = (ele.position - transform.position).normalized;
                        Vector3 forwar = ele.forward;
                        desDir = forwar * moveSpeed;
                        transform.forward = Vector3.Lerp(transform.forward, forwar, Time.fixedDeltaTime);
                    }
                }
            }
            cc.Move(desDir * Time.fixedDeltaTime);

            int addDir = moveSpeed < maxMoveSpeed ? 1 : -1;
            if (!getScoreBoard)
            {
                this.moveSpeed = Mathf.Clamp(moveSpeed + Time.fixedDeltaTime * 20 * addDir, 0, maxMoveSpeed);
            }
            else
            {
                this.moveSpeed = Mathf.Clamp(this.moveSpeed - Time.fixedDeltaTime * Random.Range(10, 60), 0, Random.Range(30, 60));
            }

        }


    }
    public bool inFlip = false;

    public void TriggerEnter(Collider other)
    {
        if (other.tag == "Flip")
        {
            inFlip = true;
            Jump gj = other.GetComponent<Jump>();
            this.jumpGravity = this.tempJumpGravity = gj.jumpGravity;
            this.jumpFallSpeed = gj.jumpFallSpeed;
            curTrunValue = 0;
            this.body.SetLocalEulerX(0);
        }


        if (other.tag == "GetJump")
        {
            this.jumpTrackTrans.Clear();
            foreach (Transform child in other.transform)
            {
                this.jumpTrackTrans.Enqueue(child);
            }

            this.moveSpeed = maxMoveSpeed;
            this.getJump = true;
            inJump = true;
            anim.SetBool("Accelerate", true);
        }


        if (other.tag == "Jump")
        {
            Jump gj = other.GetComponent<Jump>();
            if (!isFinish)
            {
                this.jumpGravity = this.tempJumpGravity = gj.jumpGravity;
                this.jumpFallSpeed = gj.jumpFallSpeed;
            }
            else
            {
                transform.Rotate(transform.up,Random.Range(-10,10));
                this.jumpGravity = this.tempJumpGravity = gj.jumpGravity + Random.Range(-10, 10);
                this.jumpFallSpeed = gj.jumpFallSpeed + Random.Range(-10, 10); ;
            }
         
            inJump = true;
        }

        if (other.tag == "Road" || other.tag == "Mount")
        {
            if (getJump)
            {
                anim.SetBool("Accelerate", false);
                this.jumpTrackTrans.Clear();
                this.moveSpeed = 20;
                //搜索前方路径点
                for (int i = curWayIndex; i < EnemySpawner.S.wayPoint.childCount; i++)
                {
                    Transform tar = WaypointScript.S.points[i];
                    Vector3 dir = (tar.position - this.transform.position).normalized;
                    float dot = Vector3.Dot(dir, transform.forward);
                    if (dot > 0)
                    {
                        curWayIndex = i;
                        this.getJump = false;
                        break;
                    }
                }
            }

            if (inJump)
            {
                inJump = false;
                tempJumpGravity = jumpGravity;
                if (isDie)
                {
                    float a = moveSpeed;
                    DOTween.To(() => this.moveSpeed, x => a = x, 0, 1).OnUpdate(() => { this.moveSpeed = a; })
                        .OnComplete(() => { isDie = false; Destroy(this.gameObject); });
                    //DOVirtual.DelayedCall(1f, () =>
                    //{
                    //    isDie = false;
                    //    Destroy(this.gameObject);
                    //});
                }
            }
            if (inFlip)
            {
                this.body.localEulerAngles = Vector3.zero;
                inFlip = false;
                tempJumpGravity = jumpGravity;
            }
        }

        if (other.tag == "Player")
        {
            if (isDie) return;
            if (other.transform.name == "Rear")
            {
                this.moveSpeed = 10;
            }
            else
            {

                if (IsFrontPlayer() && Vector3.Distance(other.transform.position, this.transform.position) > 1)
                {
                    EnemySpawner.S.allBeHitNum++;
                    UIManager.S.UpdateKillNumShow(EnemySpawner.S.allBeHitNum);
                    this.BeHit();
                }

            }
        }
        if (other.tag == "Enemy")
        {
            if (other.GetComponentInParent<Enemy>().isDie && !this.isDie)
            {
                this.BeHit();
            }
        }


        if (other.tag == "ScoreBoard")
        {
            if(isDie) return;
            if (getScoreBoard) return;
            inJump = false;
            tempJumpGravity = jumpGravity;
            anim.SetBool("Standup", true);
            this.getScoreBoard = true;
        }
        if (other.tag == "Finish")
        {
            if (isDie) return;
            this.isFinish = true;
        }
        if (other.tag == "CarSide" || other.tag == "Stone")
        {
            this.BeHit(true);
        }
    }

    private bool IsFrontPlayer()
    {
        float dotValue = Vector3.Dot((PlayerSystem.S.transform.position - this.transform.position).normalized, transform.forward);
        return dotValue < 0;
    }
    private bool IsWorldFrontPlayer()
    {
        float z = (PlayerSystem.S.transform.position - this.transform.position).z;
        return z < 0;
    }
    public void EventStopSpawnEnemy(int key, params object[] param)
    {
        this.isStopSpawn = true;
        this.maxRandSpeed = 40;
    }
    public void EventStartMove(int key, params object[] param)
    {
        //搜索前方路径点
        for (int i = 0; i < EnemySpawner.S.wayPoint.childCount; i++)
        {
            Transform tar = WaypointScript.S.points[i];
            Vector3 dir = (tar.position - this.transform.position).normalized;
            float dot = Vector3.Dot(dir, tar.right);
            if (dot > 0)
            {
                curWayIndex = i + 1;
                break;
            }
        }
        changeDirTimer = Random.Range(0.0f, 3.0f);
        this.StartMove(true);
    }
    public void StartMove(bool isStart = false)
    {
        this.startMove = true;
        if (isStart)
        {
            this.moveSpeed = 0;
            this.maxMoveSpeed = Random.Range(20, 70);
        }
        else
        {
            this.moveSpeed = 10;
            this.maxMoveSpeed = Random.Range(20, 70);
        }
    }

    public void SetDir(Vector2 dir)
    {
        this.moveDir = new Vector3(dir.x, 0, 1);
    }

    public Transform exploPoint;
    public void BeHit(bool dieSelf = false)
    {
        normalColli.gameObject.SetActive(false);
        dieColli.gameObject.SetActive(true);
        this.anim.enabled = false;
        this.cc.enabled = false;
        foreach (Rigidbody rb in this.body.GetComponentsInChildren<Rigidbody>())
        {
            rb.isKinematic = false;
            rb.GetComponent<Collider>().isTrigger = false;
            if (!dieSelf)
                rb.AddExplosionForce(Random.Range(6000, 7000), exploPoint.position , Random.Range(25, 30), Random.Range(0.1f, 0.15f));
        }

        EnemySpawner.S.RemoveEnemyFrontPlayer(this.id);
        //EnemySpawner.S.allBeHitNum++;
        //UIManager.S.UpdateKillNumShow(EnemySpawner.S.allBeHitNum);
        this.startMove = false;
       // this.moveDir = (transform.forward.normalized * Mathf.Cos(Mathf.Deg2Rad * Random.Range(-10, 10))).normalized;
     //   dieRoty = Random.Range(-3.0f, 3.0f);
       // dieRotz = Random.Range(-3.0f, 3.0f);
      //  this.moveSpeed = 100;
     //   this.tempJumpGravity = -20;
        this.isDie = true;
        this.inJump = true;
    }

    /// <summary>
    /// SendMessageFunction
    /// </summary>
    public void PlayDieSound()
    {
        if (!UIManager.S.soundOn) return;
        au.PlayOneShot(dieClips[Random.Range(0, dieClips.Length)]);
    }
}
