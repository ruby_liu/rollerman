using UnityEngine;
using DG.Tweening;
using MidnightCoder.Game;
using EZCameraShake;

public class CameraCtl : MonoBehaviour
{
    public Blur blur;
    public GameObject[] weatherFx;
    public float cameraFollowSpeed = 1;
    public float cameraRotateSpeed = 1;


    /// <summary>
    /// 跟随相机
    /// </summary>
    [HideInInspector]
    public Transform cameraFollow;
    public Transform skyboxFollow;
    /// <summary>
    /// 跟随移动对象
    /// </summary>
    [HideInInspector]
    public Transform targetFollow;
    /// <summary>
    /// 跟随旋转对象
    /// </summary>
    [HideInInspector]
    public Transform targetRot;

    public Transform followPos;
    public Transform idlePos;
    public Transform idlePos_Standard;

    public Transform jumpPos;
    public Transform carPos;
    public Transform finishPos;
    public Transform scorePos;
    public Transform readyPos;
    public Transform flipPos;

    public Transform diePos;

    Vector3 deltaPos = Vector3.zero;

    public bool startFollow = false;

    CameraShakeInstance shake;
    float magn = 1, rough = 1, fadeIn = 1f, fadeOut = 0.5f;
    Vector3 posInf = new Vector3(0.25f, 0.25f, 0.25f);
    Vector3 rotInf = new Vector3(1, 1, 1);

    private bool isStart = false;
    private bool isScore = false;
    private bool isDie = false;
    /******************Mono**********************************/
    private void Awake()
    {
        this.cameraFollow = transform.parent;
    }
    private void Start()
    {
    }

    private void Update()
    {
    }
    private void LateUpdate()
    {
        //this.UpdateFollowCameraPos();
    }
    /*********************************************************/
    /// <summary>
    /// 更新跟随相机位置
    /// </summary>
    public void UpdateFollowCameraPos()
    {
        if (!startFollow) return;
        this.cameraFollow.transform.position = Vector3.Lerp(this.cameraFollow.transform.position, this.followPos.position, Time.fixedUnscaledDeltaTime * this.cameraFollowSpeed);
        this.cameraFollow.transform.rotation = Quaternion.Lerp(Quaternion.Euler(this.cameraFollow.transform.eulerAngles), Quaternion.Euler(this.followPos.eulerAngles), Time.fixedUnscaledDeltaTime * this.cameraRotateSpeed); //new Vector3(this.cameraFollow.transform.eulerAngles.x, this.targetRot.localEulerAngles.y, this.cameraFollow.transform.eulerAngles.z))
        //this.cameraFollow.transform.rotation = Quaternion.Lerp(Quaternion.Euler(new Vector3(this.cameraFollow.transform.eulerAngles.x, this.cameraFollow.transform.eulerAngles.y, 0)), Quaternion.Euler(new Vector3(this.followPos.eulerAngles.x, this.followPos.eulerAngles.y, 0)), Time.unscaledDeltaTime * this.cameraRotateSpeed); //new Vector3(this.cameraFollow.transform.eulerAngles.x, this.targetRot.localEulerAngles.y, this.cameraFollow.transform.eulerAngles.z))

    }
    public void UpdateFollowCameraPos2()
    {
        if (isDie)
        {
            cameraFollow.position = Vector3.Lerp(cameraFollow.position, diePos.position + Vector3.up * 6 + diePos.right * 5, Time.fixedDeltaTime *100);
            cameraFollow.rotation = Quaternion.Lerp(cameraFollow.rotation,
                Quaternion.LookRotation((diePos.position - cameraFollow.position).normalized), Time.fixedDeltaTime*10 );
            //cameraFollow.LookAt(diePos);
            return;
        }
        if (isScore)
        {
            cameraFollow.parent = scorePos;
            cameraFollow.localPosition =
                Vector3.Lerp(cameraFollow.localPosition, Vector3.zero, Time.fixedDeltaTime * 10);
            cameraFollow.localRotation =
                Quaternion.Lerp(cameraFollow.localRotation, Quaternion.Euler(Vector3.zero), Time.fixedDeltaTime * 10);
            scorePos.RotateAround(targetFollow.position, targetFollow.up, Time.fixedDeltaTime * 40);
        }
        else if (!isStart)
        {
            cameraFollow.RotateAround(targetFollow.position, targetFollow.up, Time.fixedDeltaTime * 20);
        }
        else
        {
            if (followPos == null) return;
            if (Vector3.Distance(followPos.position, cameraFollow.transform.position) > 0.1f)
            {
                cameraFollow.transform.position = Vector3.Lerp(cameraFollow.transform.position, followPos.position, Time.fixedDeltaTime * this.cameraFollowSpeed);
            }
            this.cameraFollow.transform.forward = Vector3.Lerp(this.cameraFollow.transform.forward, this.followPos.transform.forward, Time.fixedDeltaTime * this.cameraRotateSpeed);
            //this.cameraFollow.transform.rotation = Quaternion.Lerp(this.cameraFollow.transform.rotation, this.followPos.rotation, Time.fixedDeltaTime * this.cameraRotateSpeed);

          //  this.skyboxFollow.position = this.cameraFollow.position;

            //this.cameraFollow.transform.position = Vector3.Lerp(this.cameraFollow.transform.position, this.followPos.position, Time.unscaledDeltaTime * this.cameraFollowSpeed);
            //this.cameraFollow.transform.rotation = Quaternion.Lerp(Quaternion.Euler(this.cameraFollow.transform.eulerAngles), Quaternion.Euler(this.followPos.eulerAngles), Time.unscaledDeltaTime * this.cameraRotateSpeed); //new Vector3(this.cameraFollow.transform.eulerAngles.x, this.targetRot.localEulerAngles.y, this.cameraFollow.transform.eulerAngles.z))

        }

    }

    //this.m_MoveCameraOffsetPos = this.movePos.position - this.targetFollow.position;
    //this.m_MoveCameraOffsetRot = this.movePos.localEulerAngles - this.targetRot.localEulerAngles;

    public void SetReady()
    {
        followPos = readyPos;
        cameraFollow.position = readyPos.position;
        cameraFollow.rotation = readyPos.rotation;
    }

    public void SetIdle()
    {
        isStart = true;
        cameraRotateSpeed = 20;
        followPos = idlePos;
    }

    public void SetCar()
    {
        followPos = carPos;
    }
    public void SetFinish()
    {
        followPos = finishPos;
        StopShake();
    }
    public void SetScore()
    {
        //followPos = scorePos;
        this.isScore = true;
    }

    public void SetDie()
    {
        this.isDie = true;
        StopShake();
    }
    public void StartShake()
    {
        shake = CameraShaker.Instance.StartShake(magn, rough, fadeIn);
        shake.DeleteOnInactive = false;
    }

    public void StopShake()
    {
        shake.DeleteOnInactive = true;
        shake.StartFadeOut(fadeOut);
        shake = null;
    }
    public void ShakeOnce()
    {
        CameraShakeInstance c = CameraShaker.Instance.ShakeOnce(5, rough, 0.1f, 0.1f);
        c.PositionInfluence = posInf;
        c.RotationInfluence = rotInf;
    }
    public void ShakeOnce2()
    {
        CameraShakeInstance c = CameraShaker.Instance.ShakeOnce(6, 6, 0, 0.8f);
        c.PositionInfluence = posInf;
        c.RotationInfluence = rotInf;
    }
    public void ShakeOnce3()
    {
        CameraShakeInstance c = CameraShaker.Instance.ShakeOnce(3, 6, 0, 0.4f);
        c.PositionInfluence = posInf;
        c.RotationInfluence = rotInf;
    }

    public void OpenWeather(bool flag = true)
    {
        Timer.S.DelayCall(1, () =>
        {
            this.weatherFx[0].SetActive(!flag);
        }, this.gameObject);
        this.weatherFx[1].SetActive(flag);
        this.weatherFx[2].SetActive(flag);
    }

    public void CloseAllWeather()
    {
        this.weatherFx[0].SetActive(false);
        this.weatherFx[1].SetActive(false);
        this.weatherFx[2].SetActive(false);
    }
    //public void SetStartMove()
    //{
    //    followPos = movePos;
    //}
    //public void SetJump()
    //{
    //    cameraFollowSpeed = 20;
    //    cameraRotateSpeed = 10;
    //    followPos = jumpPos;
    //}

}