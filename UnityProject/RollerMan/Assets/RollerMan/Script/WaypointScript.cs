﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;
using MidnightCoder.Game;
using NUnit.Framework;
using UnityEngine;

public class WaypointScript : TMonoSingleton<WaypointScript>
{
    public List<Transform> points;
    [ContextMenu("旋转点")]
    public void Fun0()
    {
        int len = transform.childCount;
        for (int i = 0; i < len; i++)
        {
            transform.GetChild(i).SetLocalEulerY(transform.localEulerAngles.y + 90);
            transform.GetChild(i).SetLocalEulerX(transform.localEulerAngles.z - 90);
        }
    }


    [ContextMenu("翻转点")]
    public void Fun1()
    {
        int len = transform.childCount;
        for (int i = 0; i < len / 2; i++)
        {
            Transform first = transform.GetChild(i);
            Transform last = transform.GetChild(len - i - 1);
            int firstId = first.GetSiblingIndex();
            int lastId = last.GetSiblingIndex();
            first.SetSiblingIndex(lastId);
            last.SetSiblingIndex(firstId);
        }
    }
    [ContextMenu("重命名")]
    public void Fun2()
    {
        int len = transform.childCount;
        for (int i = 0; i < len; i++)
        {
            transform.GetChild(i).name = "P" + i;
        }
    }

}
