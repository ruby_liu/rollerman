using UnityEngine;
using System.Collections;
using System.Security.Cryptography;

public class SplinePathWaypoints : SplinePath {
	
	public bool active = true;
	public bool show = false;
	private string m_waypointPreName = "WayPoint";
    private string m_waypointFolder = "WayPoints";
	
	private Transform parent;

    private int basePointCount;
	protected override void Awake () {
		
		if(active)
			Init();
	}
	
	// Use this for initialization
	void Start () {
		
		if(show && active)
		{
			SetRenderer(true);
        }
			
	}
	
	protected virtual void OnDrawGizmos() {
	
		if (active && (!Application.isPlaying || show))
		{
			GetWaypointNames();
			FillPath();
			FillSequence();
			DrawGizmos();
		}
		
		if(!Application.isPlaying)
		{
			//SetDrawLineToNext();
		}
		        
	}
	
	void Init()
	{
		GetWaypointNames();
		FillPath();
		FillSequence();
		parent = GameObject.Find(m_waypointFolder).transform;
		//Debug.LogError(parent.name);
		CreateNewWaypoints();
		RenamePathObjects();
	}

	void CreateNewWaypoints()
	{
		int counter = 0;
		GameObject prefab = Resources.Load(m_waypointPreName) as GameObject;
		
		foreach (Vector3 point in sequence)
		{
			
			counter ++;			
			
			//den letzten erzeugen wir nicht, da dieses die gleich Position hat wie der erste
			if (counter < sequence.Count || !loop)
			{
	            GameObject waypoint = Instantiate(prefab) as GameObject;                  
	            waypoint.transform.position = point;
	            waypoint.name = m_waypointPreName +"_"+ counter.ToString();
	            waypoint.transform.parent = parent;

                int theId = basePointCount - 1 + counter;
                if (theId > basePointCount)
                {
                    Transform before = parent.GetChild(theId-1);
                    before.forward = (waypoint.transform.position - before.position).normalized;
				}
                CopyParameters(ref waypoint, counter);
			}
		}

        parent.GetChild(parent.childCount - 1).forward = parent.GetChild(parent.childCount - 2).forward;


    }
	
	void CopyParameters(ref GameObject waypoint, int newIndex)
	{
		
		float fltOldIndex = newIndex / (steps + 1);
		
		int intOldIndex;
		
		int modIndex = newIndex % (steps + 1);
		
		if (modIndex == 0)
		{
			intOldIndex = newIndex / (steps + 1);
		}
		else
		{
			intOldIndex = 1 +(newIndex / (steps + 1));
		}
        
		
		waypoint.transform.localScale = path[intOldIndex - 1].localScale;		
		waypoint.tag = path[intOldIndex - 1].gameObject.tag;
        waypoint.GetComponent<Renderer>().enabled = show;
    }
	
	void RenamePathObjects()
	{
		foreach(Transform current in path)
		{
			current.gameObject.name = current.gameObject.name + "_original";
            Destroy(current.gameObject);
        }
	}
	
	void FillPath() 
    {				
        bool found=true;
        int counter=1;
		
		path.Clear();
		
        while (found)
        {
			GameObject go;            			
			string currentName;
            currentName = "/" + m_waypointFolder + "/WP_"  + counter.ToString();            
			go = GameObject.Find(currentName);
            
            if (go != null)
            {				                
				path.Add(go.transform);
                counter++;
            }
            else
            {
                found = false;               
            }
        }
        basePointCount = path.Count;

    }
	
	void GetWaypointNames()
    {
        AIWaypointEditor aiWaypointEditor;

        aiWaypointEditor = GetComponent("AIWaypointEditor") as AIWaypointEditor;
        if (aiWaypointEditor != null)
        {
            m_waypointPreName = aiWaypointEditor.preName ;
            m_waypointFolder = aiWaypointEditor.folderName;
        }
    }

	void SetRenderer(bool active)
	{
				
		bool found=true;
        int counter=1;
		
		path.Clear();
		
        while (found)
        {
			GameObject go;            			
			string currentName;
            currentName = "/" + m_waypointFolder + "/WP_"  + counter.ToString();            
			go = GameObject.Find(currentName);
            
            if (go != null)
            {				                
				go.GetComponent<Renderer>().enabled = active;
                counter++;
            }
            else
            {
                found = false;               
            }
            
        }      
	
	}
	
	void SetDrawLineToNext()
	{
		if (active)
		{
			
		}
		bool found=true;
        int counter=1;
		
		path.Clear();
		
        while (found)
        {
			GameObject go;            			
			string currentName;
            currentName = "/" + m_waypointFolder + "/WP_" + counter.ToString();            
			go = GameObject.Find(currentName);
            
            if (go != null)
            {				                
				DrawLineToNext drawLineToNext = go.GetComponent<DrawLineToNext>() as DrawLineToNext;
				if (drawLineToNext !=null)
				{
					if (active)
					{
	               		drawLineToNext.enabled = false;
					}
					else
					{
						drawLineToNext.enabled = true;
					}
				}
				
            }
            else
            {
                found = false;               
            }
            
        }      
				
	}
}