using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MidnightCoder.Game
{
    public class Timer : TMonoSingleton<Timer>
    {
        private List<TimerNode> timerNodeList;
        private List<int> timerNodeListDelList;
        private int curId = 1;
        void Update()
        {
            if (this.timerNodeList.Count == 0)
            {
                curId = 1;
                return;
            }

            for (int m = timerNodeList.Count - 1; m >= 0; m--)
            {
                if (this.timerNodeListDelList.Count == 0) break;
                TimerNode tn = timerNodeList[m];
                if (this.timerNodeListDelList.Contains(tn.id))
                {
                    timerNodeList.RemoveAt(m);
                    this.timerNodeListDelList.Remove(tn.id);
                }
            }
            timerNodeListDelList.Clear();

            for (int i = 0; i < timerNodeList.Count; i++)
            {
                TimerNode tn = timerNodeList[i];
                if (tn.target == null)
                {
                    this.timerNodeListDelList.Add(tn.id);
                    continue;
                }
                else
                {
                    tn.delayTime -= Time.deltaTime;
                    if (tn.delayTime <= 0)
                    {
                        tn.callback?.Invoke();
                        this.timerNodeListDelList.Add(tn.id);
                    }
                    else
                    {
                        tn.onUpdate?.Invoke();
                    }
                }
            }
        }

        public override void OnSingletonInit()
        {
            this.timerNodeList = new List<TimerNode>();
            this.timerNodeListDelList = new List<int>();
            curId = 1;
        }
        public int DelayCall(float _time, Action _call, GameObject _target, Action onUpdate = null)
        {
            TimerNode tn = new TimerNode(curId, _time, _call, _target, onUpdate);
            this.timerNodeList.Add(tn);
            return curId++;
        }
        public void Stop(int index)
        {
            this.timerNodeListDelList.Add(index);
        }

        public void StopAll()
        {
            this.timerNodeList.Clear();
            this.timerNodeListDelList.Clear();
            curId = 1;
        }
    }

    public class TimerNode
    {
        public int id;
        public float delayTime;
        public Action callback;
        public Action onUpdate;
        public GameObject target;
        public TimerNode(int id, float delayTime, Action callback, GameObject target, Action onUpdate = null)
        {
            this.id = id;
            this.delayTime = delayTime;
            this.callback = callback;
            this.onUpdate = onUpdate;
            this.target = target;
        }
    }
}
