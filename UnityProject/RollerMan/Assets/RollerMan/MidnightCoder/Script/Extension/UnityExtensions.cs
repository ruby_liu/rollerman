using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    public static Vector3 SetPosX(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(value, transform.position.y, transform.position.z);
        transform.position = newPos;
        return newPos;
    }
    public static Vector3 SetPosY(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(transform.position.x, value, transform.position.z);
        transform.position = newPos;
        return newPos;
    }
    public static Vector3 SetPosZ(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, value);
        transform.position = newPos;
        return newPos;
    }
    public static Vector3 SetLocalPosX(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(value, transform.localPosition.y, transform.localPosition.z);
        transform.localPosition = newPos;
        return newPos;
    }
    public static Vector3 SetLocalPosY(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
        transform.localPosition = newPos;
        return newPos;
    }
    public static Vector3 SetLocalPosZ(this Transform transform, float value)
    {
        Vector3 newPos = new Vector3(transform.localPosition.x, transform.localPosition.y, value);
        transform.localPosition = newPos;
        return newPos;
    }
    public static Vector3 SetEulerX(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(value, transform.eulerAngles.y, transform.eulerAngles.z);
        transform.eulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetEulerY(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(transform.eulerAngles.x, value, transform.eulerAngles.z);
        transform.eulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetEulerZ(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, value);
        transform.eulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetLocalEulerX(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(value, transform.localEulerAngles.y, transform.localEulerAngles.z);
        transform.localEulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetLocalEulerY(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(transform.localEulerAngles.x, value, transform.localEulerAngles.z);
        transform.localEulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetLocalEulerZ(this Transform transform, float value)
    {
        Vector3 newEuler = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, value);
        transform.localEulerAngles = newEuler;
        return newEuler;
    }
    public static Vector3 SetScaleX(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(value, transform.lossyScale.y, transform.lossyScale.z);
        transform.localScale = newScale;
        return newScale;
    }
    public static Vector3 SetScaleY(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(transform.lossyScale.x, value, transform.lossyScale.z);
        transform.localScale = newScale;
        return newScale;
    }
    public static Vector3 SetScaleZ(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(transform.lossyScale.x, transform.lossyScale.y, value);
        transform.localScale = newScale;
        return newScale;
    }
    public static Vector3 SetLocalScaleX(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(value, transform.localScale.y, transform.localScale.z);
        transform.localScale = newScale;
        return newScale;
    }
    public static Vector3 SetLocalScaleY(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(transform.localScale.x, value, transform.localScale.z);
        transform.localScale = newScale;
        return newScale;
    }
    public static Vector3 SetLocalScaleZ(this Transform transform, float value)
    {
        Vector3 newScale = new Vector3(transform.localScale.x, transform.localScale.y, value);
        transform.localScale = newScale;
        return newScale;
    }
}


public static class DictionaryExtensions
{
    public static TValue TryGetValue<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
    {
        TValue value = default(TValue);
        dict.TryGetValue(key, out value);
        return value;
    }
}
